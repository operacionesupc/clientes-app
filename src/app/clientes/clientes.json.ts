import {Cliente} from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: 'Nicolas', apellido: 'Cruz', email: 'nicolasbncruz@gmail.com', createAt: '2019-12-11'},
  {id: 2, nombre: 'Mr John', apellido: 'Doe', email: 'mrjonesdoe@gmail.com', createAt: '2019-12-12'},
  {id: 3, nombre: 'Linus', apellido: 'Torvalds', email: 'linustorvald@gmail.com', createAt: '2019-11-11'},
  {id: 4, nombre: 'Rasmus', apellido: 'Hodor', email: 'rasmushodor@gmail.com', createAt: '2019-12-13'},
  {id: 5, nombre: 'Erick', apellido: 'Gamma', email: 'erickgamma@gmail.com', createAt: '2019-11-14'},
  {id: 6, nombre: 'Richard', apellido: 'Helm', email: 'richardhelm@gmail.com', createAt: '2019-11-15'},
  {id: 7, nombre: 'Ralph', apellido: 'Johnson', email: 'ralphjhonson@gmail.com', createAt: '2019-11-16'},
  {id: 8, nombre: 'Bombasto', apellido: 'Viseris', email: 'bombastoviseris@gmail.com', createAt: '2019-11-17'},
  {id: 9, nombre: 'Dr James', apellido: 'Gosling', email: 'drjamesgosling@gmail.com', createAt: '2019-11-18'},
  {id: 10, nombre: 'Magma', apellido: 'Lee', email: 'magmalee@gmail.com', createAt: '2019-11-19'},
  {id: 11, nombre: 'Tornado', apellido: 'Roe', email: 'tornadoroe@gmail.com', createAt: '2019-11-20'},
];
